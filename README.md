Hiawatha
========
Hiawatha is an open source webserver with security, easy to use and lightweight as the three key features. It supports among others (Fast)CGI, IPv6, URL rewriting and reverse proxy and has security features no other webserver has, like blocking SQL injections, XSS, CSRF and exploit attempts. Hiawatha runs perfectly on Linux, BSD and MacOS X.

The Hiawatha webserver has been written by Hugo Leisink \<hugo@leisink.net\>. More information about the Hiawatha webserver can be found at https://hiawatha.leisink.net/.

Related projects
----------------
The Hiawatha Monitor is a monitoring tool for Hiawatha. It helps you to keep track of all your Hiawatha installations. It's a PHP webapplication and requires a MySQL database and the cron daemon for periodic downloading of statistical information from the webservers it monitors. More information about the Hiawatha Monitor can be found at https://hiawatha.leisink.net/monitor.

Other interesting projects can be found at https://gitlab.com/hsleisink.
